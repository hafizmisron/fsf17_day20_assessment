// Database initialization
module.exports = function(Sequelize) {

    const MYSQL_USERNAME    = "root";
    const MYSQL_PASSWORD    = "1CapitalH";
    const MYSQL_DBNAME      = "grocery";

    var connection = new Sequelize(
        MYSQL_DBNAME,
        MYSQL_USERNAME,
        MYSQL_PASSWORD,
        {
            host: 'localhost',
            logging: console.log,
            dialect: 'mysql',
            pool: {
                max: 5,
                min: 0,
                idle: 10000
            }
        }
    );

    // Connection authentication
    connection
        .authenticate(function(){
            console.log("DB connected");
        })
        .catch(function(err){
            console.log(err);
        });

    return connection;
}