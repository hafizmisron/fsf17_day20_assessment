var express = require("express");
var path = require("path");
var bodyParser = require("body-parser");

var app = express();

const NODE_PORT = 3000;
const CLIENT_FOLDER = path.join(__dirname, "..", "client");

app.use(express.static(CLIENT_FOLDER));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

require("./route")(app);

app.listen(NODE_PORT, function() {
    console.log("App is listening at " + NODE_PORT);
});