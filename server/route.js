var Sequelize = require("sequelize");
var connection = require("./database")(Sequelize);

const API_GROCERY_ENDPOINT = "/api/grocery"

// Load db models
 var GroceryList = require("./models/grocerylist")(connection, Sequelize);

module.exports = function(app) {
    
    /***
     * ROUTES-------------------------------------------------------------------------
     * GET  /api/grocery/test                           -- Test API
     * GET  /api/grocery/searchBrand/                   -- Search for brand
     * GET  /api/grocery/searchName/                    -- Search for product name
     * GET  /api/grocery/searchAll/                     -- Search both brand and prod name
     * 
     * PUT  /api/grocery/
     * -------------------------------------------------------------------------------
     */
     
     app.get(API_GROCERY_ENDPOINT + "/test", function(req, res) {
        
        GroceryList 
            .findAll({
                limit: 20
            })
            .then(function(result) {
                res
                    .status(200)
                    .json(result);
            })
            .catch(function(err) {
                res
                    .status(500)
                    .json(err);
            });

     });

     app.get(API_GROCERY_ENDPOINT + "/searchBrand/:searchString", function(req, res) {
         console.log("search string: " + req.params.searchString);
        
            GroceryList 
                .findAll({
                    where: {
                        brand: {
                            $like: "%" + req.params.searchString + "%"
                        }
                    },
                    limit: 20
                })
                .then(function(result) {
                    res
                        .status(200)
                        .json(result);
                })
                .catch(function(err) {
                    res
                        .status(500)
                        .json(err);
                });

     });

     app.get(API_GROCERY_ENDPOINT + "/searchName/:searchString", function(req, res) {
        
        GroceryList 
            .findAll({
                where: {
                    name: {
                        $like: "%" + req.params.searchString + "%"
                    }
                },
                limit: 20
            })
            .then(function(result) {
                res
                    .status(200)
                    .json(result);
            })
            .catch(function(err) {
                res
                    .status(500)
                    .json(err);
            });

     });

     app.get(API_GROCERY_ENDPOINT + "/searchAll/:searchString", function(req, res) {
        
        GroceryList 
            .findAll({
                where: {
                    $or: [{
                            name: {
                                $like: "%" + req.params.searchString + "%"
                            }
                        },
                        {
                            brand: {
                                $like: "%" + req.params.searchString + "%"
                        }
                    }]
                },
                limit: 20
            })
            .then(function(result) {
                res
                    .status(200)
                    .json(result);
            })
            .catch(function(err) {
                res
                    .status(500)
                    .json(err);
            });

     });

     app.get(API_GROCERY_ENDPOINT + "/getProduct/:productid", function(req, res) {
        
        GroceryList 
            .find({
                where: {
                    id: req.params.productid
                }
            })
            .then(function(result) {
                res
                    .status(200)
                    .json(result);
            })
            .catch(function(err) {
                res
                    .status(500)
                    .json(err);
            });

     });

     app.put(API_GROCERY_ENDPOINT + "/updateProduct/:id", function(req, res) {

        GroceryList 
            .update({
                name: req.body.product.name,
                brand: req.body.product.brand,
                upc12: req.body.product.upc12
            }, {
                where: {
                    id: req.params.id
                }
            })
            .then(function(result) {
                res
                    .status(200)
                    .json(result);
            })
            .catch(function(err) {
                res
                    .status(500)
                    .json(err);
            });

    });

}