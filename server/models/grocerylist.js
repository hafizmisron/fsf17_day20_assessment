module.exports = function(conn, Sequelize) {

    var GroceryList = conn.define("grocery_list", {

        id: {

            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            unique: true

        },
        upc12: {

            type: Sequelize.BIGINT(12),
            allowNull: false

        },
        brand: {

            type: Sequelize.STRING,
            allowNull: false

        },
        // Roger. Added name as per the HTML input field
        name: {
            type: Sequelize.STRING,
            allowNull: true
        }
    }, {

        tableName: "grocery_list",
        timestamps: false
    });

    return GroceryList;
}