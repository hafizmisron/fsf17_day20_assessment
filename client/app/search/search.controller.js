(function() {
    angular
        .module("Day20App")
        .controller("SearchCtrl", SearchCtrl);
    
    SearchCtrl.$inject = ["$state", "orderByFilter", "GroceryService"];
    

    function SearchCtrl($state, orderBy, GroceryService) {
        var vm = this;
        vm.productname = "";
        vm.brand = "";
        vm.searchList = null;
        vm.resultShow = false;
        vm.text = "";

        vm.property = "name";
        vm.reverse = true;
        
        vm.submit = submit;
        vm.sortBy = sortBy;
        vm.edit = edit;

        function submit() {
            if(vm.productname != "" && vm.brand != "") {
                vm.text = "";
                console.log("Both fields filled");
                GroceryService
                    .searchAll(vm.productname)
                    .then(function(result) {
                        vm.searchList = result;
                        vm.searchList = orderBy(vm.searchList, vm.property, vm.reverse);
                        vm.resultShow = true;
                    })
                    .catch(function(err) {
                        console.log(err);
                    })
            }
            else if(vm.productname != "") {
                vm.text = "";
                GroceryService
                    .searchName(vm.productname)
                    .then(function(result) {
                        vm.searchList = result;
                        vm.searchList = orderBy(vm.searchList, vm.property, vm.reverse);
                        vm.resultShow = true;
                    })
                    .catch(function(err) {
                        console.log(err);
                    })

            }
            else if (vm.brand != "") {
                vm.text = "";
                GroceryService
                    .searchBrand(vm.brand)
                    .then(function(result) {
                        vm.searchList = result;
                        vm.searchList = orderBy(vm.searchList, vm.property, vm.reverse);
                        vm.resultShow = true;
                    })
                    .catch(function(err) {
                        console.log(err);
                    })
            }
            else {
                // if both fields are empty...
                console.log("Both fields empty");
                vm.text = "Search not run, either one or both fields needs to be filled.";
            }
                
        }

        function sortBy(property) {
            vm.reverse = (property !== null && vm.property === property) ? !vm.reverse : false;
            vm.property = property;
            vm.searchList = orderBy(vm.searchList, vm.property, vm.reverse);
        }

        function edit(id) {
            $state.go("Edit", {id: id});
        }
    }
    
})();