(function () {

    angular
        .module("Day20App")
        .service("GroceryService", GroceryService);
    
    GroceryService.$inject = ["$http"];

    function GroceryService($http) {
        var service = this;
        service.getAll = getAll;
        service.searchBrand = searchBrand;
        service.searchName = searchName;
        service.searchAll = searchAll;
        service.getProduct = getProduct;
        service.updateProduct = updateProduct;

        function getAll() {
            return ($http.get("/api/grocery/test")
                .then(function(result) {
                    return(result.data);
                }));
        }

        function searchBrand(searchString) {
            return ($http.get("/api/grocery/searchBrand/" + searchString)
                .then(function(result) {
                    return(result.data);
                }));
        }

        function searchName(searchString) {
            return ($http.get("/api/grocery/searchName/" + searchString)
                .then(function(result) {
                    return(result.data);
                }));
        }

        function searchAll(searchString) {
            return ($http.get("/api/grocery/searchAll/" + searchString)
                .then(function(result) {
                    return(result.data);
                }));
        }

        function getProduct(productid) {
            return ($http.get("/api/grocery/getProduct/" + productid)
                .then(function(result) {
                    return(result.data);
                    console.log(result.data);
                }));
        }

        function updateProduct(prodid, proddata) {
            return($http.put("/api/grocery/updateProduct/" + prodid, proddata)
                .then(function(result) {
                    return result.data;
                }));
        }

    }
})();