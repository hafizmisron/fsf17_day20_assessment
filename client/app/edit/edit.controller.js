(function() {
    angular
        .module("Day20App")
        .controller("EditCtrl", EditCtrl);
    
    EditCtrl.$inject = ["$state", "$stateParams", "$http", "GroceryService"];
    

    function EditCtrl($state, $stateParams, $http, GroceryService) {
        var vm = this;

        vm.submit = submit;
        vm.cancel = cancel;

        var product = null;

        GroceryService
            .getProduct($stateParams.id)
            .then(function(result) {

                product = result;
                vm.brand = product.brand;
                vm.productname = product.name;
                vm.upc12 = product.upc12;
            })
            .catch(function(err) {
                console.log(err);
            });

        
        function submit() {
            var update_data = {
                "product" : {
                    "name": vm.productname,
                    "brand": vm.brand,
                    "upc12": vm.upc12
                }
                    
            };

            GroceryService
                .updateProduct($stateParams.id, update_data)
                .then(function(result) {
                    // Success. Go back to prev page.
                    $state.go("Search");
                })
                .catch(function(err) {
                    console.log(err);
                })
        }

        function cancel() {
            $state.go("Search");
        }


    }
    
})();