(function () {
    angular
        .module("Day20App")
        .config(uirouterAppConfig);
    
    uirouterAppConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function uirouterAppConfig($stateProvider, $urlRouterProvider){
        $stateProvider
            .state('Search', {
                url: '/Search',
                templateUrl: "app/search/search.html",
                controller : 'SearchCtrl',
                controllerAs : 'ctrl'
            })
            .state('Edit', {
                url : '/Edit',
                templateUrl: "app/edit/edit.html",
                controller : 'EditCtrl',
                params: {'id': null},
                controllerAs : 'ctrl'
            })

        $urlRouterProvider.otherwise("/Search");
    
    }

})();